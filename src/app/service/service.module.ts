import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServiceRoutingModule } from './service-routing.module';
import { ServiceComponent } from './service/service.component';
import { ServiceS1Component } from './service-s1/service-s1.component';
import { ServiceS2Component } from './service-s2/service-s2.component';


@NgModule({
  declarations: [
    ServiceComponent,
    ServiceS1Component,
    ServiceS2Component
  ],
  imports: [
    CommonModule,
    ServiceRoutingModule
  ]
})
export class ServiceModule { }
